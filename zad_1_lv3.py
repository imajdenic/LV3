# -*- coding: utf-8 -*-
"""
Created on Sun Dec 06 13:13:38 2015

@author: ilija
"""

import pandas as pd
import numpy as np
#1.Kojih 5 automobila ima najveću potrošnju? (koristite funkciju sort)
mtcars=pd.read_csv('mtcars.csv')
print mtcars
sortiranje=mtcars.sort(['mpg'])
print sortiranje
print sortiranje.tail(5)
#2. Koja tri automobila s 8 cilindara imaju najmanju potrošnju?
print mtcars.cyl==8
print mtcars[mtcars.cyl==8]
sortiranje_1=mtcars[mtcars.cyl==8].sort(['mpg'])
print sortiranje_1
print sortiranje_1.head(3)
#3. Kolika je srednja potrošnja automobila sa 6 cilindara?
print mtcars[mtcars.cyl==6]
sr_potrosnja=mtcars[mtcars.cyl==6]
print sr_potrosnja['mpg'].mean()
#4. Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs?
print mtcars[(mtcars.cyl==4) & (mtcars.wt>=2.0) & (mtcars.wt<=2.2)]
sr_potrosnja1=mtcars[(mtcars.cyl==4) & (mtcars.wt>=2.0) & (mtcars.wt<=2.2)]
print sr_potrosnja1['mpg'].mean()
#5. Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka?
print mtcars[mtcars.am==0]
automatik=mtcars[mtcars.am==0]
print automatik['am'].count()
print mtcars[mtcars.am==1]
rucni=mtcars[mtcars.am==1]
print rucni['am'].count()
#6. Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga?
print mtcars[(mtcars.am==0) & (mtcars.hp>100)]
preko_sto=mtcars[(mtcars.am==0) & (mtcars.hp>100)]
print preko_sto['car'].count()
#7. Kolika je masa svakog automobila u kilogramima?
print mtcars[['car', 'wt']]
mtcars['kg']=mtcars.wt*0.45359237*1000
print mtcars[['car','kg']]



