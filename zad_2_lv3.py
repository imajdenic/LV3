# -*- coding: utf-8 -*-
"""
Created on Mon Dec 07 10:30:17 2015

@author: ilija
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
mtcars=pd.read_csv('mtcars.csv')
print mtcars
#1. Pomoću barplot-a prikažite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara.
cil4=mtcars[mtcars.cyl==4]
cil6=mtcars[mtcars.cyl==6]
cil8=mtcars[mtcars.cyl==8]
a=pd.DataFrame({'cil4': cil4.mpg, 'cil6': cil6.mpg , 'cil8':cil8.mpg}, columns=['cil4', 'cil6','cil8'])
a.plot(kind='bar')
#2. Pomoću boxplot-a prikažite na istoj slici distribuciju težine automobila s 4, 6 i 8 cilindara.
b=pd.DataFrame({'cil4': cil4.mpg, 'cil6': cil6.mpg , 'cil8':cil8.mpg}, columns=['cil4', 'cil6','cil8'])
b.plot(kind='box')